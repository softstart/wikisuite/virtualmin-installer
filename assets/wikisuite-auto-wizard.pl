#!/usr/bin/perl

package virtual_server;
if (!$module_name) {
	$main::no_acl_check++;
	$ENV{'WEBMIN_CONFIG'} ||= "/etc/webmin";
	$ENV{'WEBMIN_VAR'} ||= "/var/webmin";
	if ($0 =~ /^(.*)\/[^\/]+$/) {
		chdir($pwd = $1);
		}
	else {
		chop($pwd = `pwd`);
		}
	$0 = "$pwd/wikisuite-auto-wizard.pl";
	require './virtual-server-lib.pl';
	$< == 0 || die "auto-wizard.pl must be run as root";
	}
@OLDARGV = @ARGV;
&set_all_text_print();

if ( $config{'wizard_run'} == 1 ) {
    print "Wizard already executed, exiting ...\n";
    exit 0;
}

require './wizard-lib.pl';

my $memory;
%$memory = (
	'preload' => 0,
);
$err = &wizard_parse_memory($memory);
$err = 'No error' if (!$err);
print "Memory: $err\n";

my $db;
%$db = (
	mysql => 1,
	postgres => 0,
);
$err = &wizard_parse_db($db);
$err = 'No error' if (!$err);
print "Db: $err\n";

my $mysql;
%$mysql = (
	mypass => &random_password(16),
);
$err = &wizard_parse_mysql($mysql);
$err = 'No error' if (!$err);
print "Mysql: $err\n";
print "Mysql root Password: ".$mysql->{'mypass'}."\n";

my $mysize;
%$mysize = (
	mysize => 'huge',
);
$err = &wizard_parse_mysize($mysize);
$err = 'No error' if (!$err);
print "Mysize: $err\n";

my $hashpass;
%$hashpass = (
	hashpass => 1,
);
$err = &wizard_parse_hashpass($hashpass);
$err = 'No error' if (!$err);
print "Hashpass: $err\n";

my $defdom;
%$defdom = (
	'defdom' => true,
	'defhost' => $ENV{'WIKISUITE_DEFHOST'},
	'defssl' => 2,
);
$err = &wizard_parse_defdom($defdom);
$err = 'No error' if (!$err);
print "Defdom: $err\n";

$config{'wizard_run'} = 1;
&save_module_config();

print "done\n";
